from preview_generator.manager import PreviewManager
import os
import shutil
from pathlib import Path

class GeneratePreview:
      cache_path = '/root/myglobal/coolglobal/homework_previews'
      #filepath =  '/home/ubuntu/Documents/Global/uploaded_homeworks'

      def createPreview(self,image_path, filename):
          pdf_or_odt_to_preview_path = image_path
          manager = PreviewManager(self.cache_path, create_folder= True)
          path_to_preview_image = manager.get_jpeg_preview(pdf_or_odt_to_preview_path, width=900, height=1200)
          newname  = os.path.splitext(filename)[0] + '.jpeg'
          os.rename(path_to_preview_image, os.path.join(self.cache_path, newname))
     

gn = GeneratePreview()
#gn.createPreview('/home/ubuntu/Documents/Global/uploaded_homeworks/test.txt', 'test.txt')     