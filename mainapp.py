import falcon
from FileUpload import Resource
from FileDownload import HomeworkDownload
from DownPrev import DownloadThumbnail
from rest_api import Parentlogin
from rest_api import Homework
from rest_api import GetStudDetail
from falcon_multipart.middleware import MultipartMiddleware

api  = falcon.API(middleware=[MultipartMiddleware()])
api.req_options.auto_parse_form_urlencoded = True
homework = Homework()
images = Resource('uploaded_homeworks')
downs = HomeworkDownload('uploaded_homeworks')
downprev = DownloadThumbnail('/root/myglobal/coolglobal/homework_previews')
parent_login = Parentlogin()
dtl = GetStudDetail()
api.add_route('/downs',downs)
api.add_route('/getstuddetail/{mobileno}', dtl, suffix='mobile')
api.add_route('/file_upload', images)
api.add_route('/downprev/{filename}',downprev,suffix='work')
api.add_route('/homework', homework)
api.add_route('/homework/{grade}/{section}', homework, suffix='homework')
api.add_route('/parent_login', parent_login)
api.add_route('/parent_login/{mobileno}', parent_login, suffix='mobile')
