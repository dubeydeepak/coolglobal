import psycopg2
import sys
import json
from psycopg2.extras import Json
from psycopg2.extras import json as psycop_json
from psycopg2.extras import RealDictCursor

class dbutil:
     con = None
     cur = None
     def _connect(self):
         self.con = psycopg2.connect(database="global", user="postgres",password="raspberrypi@pi",host="127.0.0.1",port="5432", cursor_factory=RealDictCursor)
         self.con.set_session(autocommit=True)
         self.cur = self.con.cursor()
         print("Database connected successfully")
      
     def _close():
         self.con.close()

     def update(self, tablename,coil_to_update,condition_col,updatedvalue,condition_value):
          sql = """UPDATE {0}
                SET {1} ='{3}'
                WHERE {2} ='{4}'""".format(tablename,coil_to_update,condition_col,updatedvalue,condition_value)

          self.cur.execute(sql)


     def  selectdatafromtableThreeConditions(self,table_name, columnnamea,columnnameb, columnnamec, valuea,valueb,valuec):
#         cur = self.con.cursor()
         query = """SELECT * FROM {0} WHERE  {1}='{4}' AND {2}='{5}' AND {3}='{6}';""".format(table_name, columnnamea,columnnameb, columnnamec, valuea,valueb,valuec)
         print(query)
         self.cur.execute(query)
         fetched_data = self.cur.fetchall()
         if fetched_data ==  []:
             return None
         data = json.dumps(fetched_data, indent=2)
         print(data) 
         return data 


     def  selectdatafromtableTwoConditions(self,table_name, columnnamea,columnnameb, valuea,valueb):
#         cur = self.con.cursor()
         query = """SELECT * FROM {0} WHERE  {1}='{3}' AND {2}='{4}';""".format(table_name, columnnamea,columnnameb, valuea,valueb)
         print(query)
         self.cur.execute(query)
         fetched_data = self.cur.fetchall()
         if fetched_data == []:
             return None
         #data = json.dumps(fetched_data, indent=2)      
         print(fetched_data) 
         return fetched_data     
     
     def  selectalldatafromtable(self, tablename):
#         cur = self.con.cursor()
         query = """SELECT * FROM {0};""".format(tablename)
         print(query)
         self.cur.execute(query)
         fetched_data = self.cur.fetchall()
         if fetched_data == []:
             return None
         data = json.dumps(fetched_data, indent=2)      
         print(data) 
         return data 


     def  selectdatafromtableoneCondition(self, tablename,columnname, columvalue):
#         cur = self.con.cursor()
         query = """SELECT * FROM {0} WHERE  {1}='{2}';""".format(tablename,columnname,columvalue)
         print(query)
         self.cur.execute(query)
         fetched_data = self.cur.fetchall()
         #print(fetched_data) 
         if fetched_data == []:
             return None
         #data = json.dumps(, indent=2)   
         print("going to print the data:  ")   
         return fetched_data[0]
         #self.con.close()
     

     def  selectalldatafromtableoneCondition(self, tablename,columnname, columvalue):
#         cur = self.con.cursor()
         query = """SELECT * FROM {0} WHERE  {1}='{2}';""".format(tablename,columnname,columvalue)
         print(query)
         self.cur.execute(query)
         fetched_data = self.cur.fetchall()
         #print(fetched_data) 
         if fetched_data == []:
             return None
         #data = json.dumps(, indent=2)   
         print("going to print the data:  ")   
         return fetched_data




     def  insertJsonData(self, table_name, json_data):
            record_list = json_data
            print(record_list)
            self._insertData(table_name, record_list )
    
     def  insertJsonDataFile(self, table_name, filename):
         with open(filename) as json_data:
            record_list = json.load(json_data)
            self._insertData(table_name, record_list )
    
     def _insertData(self, table_name, record_list):
       # print ("\nrecords:", record_list)
       # print ("\nJSON records object type:", type(record_list)) # should return "<class 'list'>"
        sql_string = 'INSERT INTO {} '.format( table_name )
        if type(record_list) == list:
            first_record = record_list[0]
            columns = list(first_record.keys())
        else:
            print ("Needs to be an array of JSON objects")
            sys.exit()
        sql_string += "(" + ', '.join(columns) + ")\nVALUES "
        for i, record_dict in enumerate(record_list):
            values = []
            for col_names, val in record_dict.items():
                if type(val) == str:
                    val = val.replace("'", "''")
                    val = "'" + val + "'"
                if "'" not in val:    
                   values += ["'" + str(val) + "'"]
                else:
                   values += [str(val)]

            sql_string += "(" + ', '.join(values) + "),\n"
        sql_string = sql_string[:-2] + ";"
        #print ("\nSQL string:")
        print (sql_string)
        self.con.cursor().execute(sql_string)
        print("data inserted successfully")


     


db =  dbutil()
db._connect()

#db.insertJsonDataFile("students", "I-R.json")
#db.selectdatafromtableoneCondition("parentslogin", "mobile_no", "9628010323")
