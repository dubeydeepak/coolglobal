import io
import os
import uuid
import mimetypes
import falcon
import json
from prev import GeneratePreview




class Resource(object):

        _CHUNK_SIZE_BYTES = 4096
        _storage_path = ""

        def __init__(self, storage_path):
            self._storage_path = storage_path

        def on_get(self,req,resp):
            resp.body = req.query_string
            


        def on_post(self, req, resp):
            image = req.get_param("profilePic")
            # image_type = req.get_param("profilePic").type        
            ext = mimetypes.guess_extension(req.content_type)
            filename =  image.filename
            image_path = os.path.join(self._storage_path, filename)
            with open(image_path, "wb") as image_file:
                while True:
                    chunk = image.file.read(4096)
                    image_file.write(chunk)
                    if not chunk:
                        break
            resp.status = falcon.HTTP_200
            crtpr = GeneratePreview()
            crtpr.createPreview(image_path, filename)
            resp.location = filename
            resp.body = json.dumps("{name:" + image_path + "}")
            

