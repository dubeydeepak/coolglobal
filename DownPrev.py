import falcon
import mimetypes
import os


class DownloadThumbnail(object):
     _storage_path = "homework_previews" 

     def __init__(self, storage_path):
            self._storage_path = storage_path


     def on_get_work(self,req,resp,filename):
         #filename =  req.get_param("homework_name")
         print(self._storage_path)
         resp.content_type = mimetypes.guess_type(filename)[0]
         newname  = os.path.splitext(filename)[0] + '.jpeg'
         image_path = os.path.join(self._storage_path, newname)
         with open(image_path, 'rb') as f:
             resp.body = f.read()
