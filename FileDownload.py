import falcon
import mimetypes
import os

class HomeworkDownload(object):
     storagepath = "" 

     def __init__(self, storage_path):
            self._storage_path = storage_path


     def on_post(self,req,resp):
         filename =  req.get_param("homework_name")
         resp.content_type = mimetypes.guess_type(filename)[0]
         image_path = os.path.join(self._storagepath, filename)
         with open(image_path, 'rb') as f:
             resp.body = f.read()

         



         