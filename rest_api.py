import falcon
import json
import io
import shutil
import os
import databaseutil
from databaseutil import dbutil
from random import choice
from string import digits
#from messaging import message
import requests
#import unirest


class Homework(object):
    

      storage_path = "./uploaded_homeworks"
      _CHUNK_SIZE_BYTES = 4096
      def prepareNotification(self,req_data):
         token_dict_list = databaseutil.db.selectdatafromtableTwoConditions("students","grade","section",req_data["grade"],req_data["section"])
         tokens = []
         for dict in token_dict_list:
           if dict["fcm_token"] is not None:
              tokens.append(dict["fcm_token"])
           elif dict["fcm_tokenb"] is not None:  
              tokens.append(dict["fcm_tokenb"])
         title =  req_data["subject_name"] + " Homework!!"
         #m = message()
         #m.sendNotification(title,tokens,None,req_data)



      
      def on_post(self,req,resp):
          reqbody = req.media
          json_list = reqbody["homework"]
          databaseutil.db.insertJsonData("homework",json_list)
          resp.status = falcon.HTTP_200
          req_data == json_list[0]
          self.prepareNotification(req_data)
          resp.body = json.dumps("{result:" + "Homework sent successfully." + "}")
          
      def  on_get_homework(self,req,resp,grade,section):
           resp.status = falcon.HTTP_200
           resp.media  = databaseutil.db.selectdatafromtableTwoConditions("homework","grade","section",grade,section)
           





#object
class Parentlogin(object):

      def callback_function(response):
         print(response.code)# The unparsed response
      
      def sendMobileTextMessage(self, mobileno, otp):
           query_string = """http://sms.sunstechit.com/app/smsapi/index.php?key=25D5BB5524EEB5&campaign=0&routeid=13&type=text&senderid=SGSPBH&contacts={0}&msg=Welcome to Sanskar Global!!\n Your New Pin for Login is: {1}""".format(mobileno,otp)
           print(query_string)
           r =requests.get(query_string)
        #r = unirest.get(query_string, callback=callback_function)
           print(r.status_code)    
        #print(otp)

      def insertandsendOTPtoMobileno(self, mobileno, otp):
         data_set = {}
         data_set['mobile_no'] =  mobileno
         data_set['otp'] =  otp
         lst = []
         lst.append(data_set)
         databaseutil.db.insertJsonData("parentslogin",lst)
         th = threading.Thread(target=self.sendMobileTextMessage, args=(mobileno,otp, ))
         th.start()
 
         self.sendMobileTextMessage(mobileno,otp)
     
      def on_post(self,req,resp):
          reqbody = req.media
          print(type(reqbody))
          fcm_token = reqbody["fcm_token"]
          mobile_no = reqbody["mobile_no"]
          device_token = reqbody["device_token"]
          otp = reqbody["otp"]
          get_data = databaseutil.db.selectdatafromtableoneCondition("parentslogin","mobile_no", mobile_no)
          if get_data is None:
               resp.status = falcon.HTTP_404
               resp.body = json.dumps("{result:" + "Mobile no does not match" + "}")
               return 
          otp_matched = get_data["otp"]
          if otp_matched == otp:
              max_all = get_data
              if max_all["maxallowed"] == '1':
                  databaseutil.db.update("parentslogin","device_tokenb","otp",device_token,otp_matched) 
                  databaseutil.db.update("parentslogin","fcm_tokenb","otp",fcm_token,otp_matched) #need to update the parent login table
                  databaseutil.db.update("parentslogin","maxallowed","otp","2",otp_matched)   
                  databaseutil.db.update("students","fcm_tokenb","mobileno",fcm_token,mobile_no)   
                  resp.status = falcon.HTTP_200
                  res_dict = {}
                  res_dict["result"] = "Parent Mobileno is Verified successfully."
                  res_dict["code"] = "200"
                  resp.media = res_dict
                  #need to update the parent login table
              else:
                  databaseutil.db.update("parentslogin","device_token","otp",device_token,otp_matched) 
                  databaseutil.db.update("parentslogin","fcm_token","otp",fcm_token,otp_matched)   
                  databaseutil.db.update("parentslogin","maxallowed","otp","1",otp_matched)   
                  databaseutil.db.update("students","fcm_token","mobileno",fcm_token,mobile_no)   
                  resp.status = falcon.HTTP_200
                  res_dict = {}
                  res_dict["result"] = "Parent Mobileno is Verified successfully."
                  res_dict["code"] = "200"
                  resp.media = res_dict                  
                  return    
          else:
               resp.status = falcon.HTTP_500
               resp.body = json.dumps("{result:" + "OTP does not match.Please try again to login" + "}")
               return 

      def generaterandonumber(self):
       	     code = list()
             for i in range(6):
                 code.append(choice(digits))
             random_digits = ''.join(code)
             print("rand code is : " + random_digits)
             return random_digits
      def on_get(self,req,resp):
          resp.body = json.dumps("{result:" + "Mobileno not registered with us.Please Verify" + "}")
          resp.status = falcon.HTTP_200

      def  on_get_mobile(self,req,resp,mobileno):
           get_data = databaseutil.db.selectdatafromtableoneCondition("parentslogin","mobile_no",mobileno)
           if get_data is None :
              check_mobileno = databaseutil.db.selectdatafromtableoneCondition("students","mobileno",mobileno)
              if check_mobileno is None:
                 resp.status = falcon.HTTP_404
                 resp.body = json.dumps("{result:" + "Mobileno not registered with us.Please Verify" + "}")
                 return
              self.insertandsendOTPtoMobileno(mobileno,self.generaterandonumber())
              resp.status = falcon.HTTP_200
              res_dict = {}
              res_dict["result"] = "OTP sent to your registered Mobileno"
              res_dict["code"] = "200"
              resp.media = res_dict
           else :
             max_all = get_data["maxallowed"]
             if max_all is None or max_all == '0':
               	otp  = self.generaterandonumber()
               	databaseutil.db.update("parentslogin","otp","mobile_no",otp,mobileno)
                self.sendMobileTextMessage(mobileno,otp)
                resp.status = falcon.HTTP_200
                res_dict = {}
                res_dict["result"] = "OTP sent to your registered Mobileno"
                res_dict["code"] = "200"
                resp.media = res_dict           
             elif max_all == '1':
               dig = self.generaterandonumber()
               databaseutil.db.update("parentslogin","otp","mobile_no",dig,mobileno)
               self.sendMobileTextMessage(mobileno,dig)
               resp.status = falcon.HTTP_200
               res_dict = {}
               res_dict["result"] = "OTP sent to your registered Mobileno"
               res_dict["code"] = "200"
               resp.media = res_dict                     
             else :
               resp.body = json.dumps("{result:" + "You have completed your max allowed logins" + "}")
               return
 









class GetStudDetail(object):
        """docstring for ClassName"""
      


        def  on_get_mobile(self,req,resp,mobileno):
              resp.media = databaseutil.db.selectalldatafromtableoneCondition("students","mobileno",mobileno)
              resp.status = falcon.HTTP_200

                
                
